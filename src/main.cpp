#include <Arduino.h>
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "soc/ledc_reg.h"
#include <Streaming.h>

// #define SIGNAL_PIN GPIO_NUM_12
#define STEP_FREQUENCY 20000
#define SIN_TABLE_SIZE 100
#define SIN_FREQUENCY 75.0
#define PIN_TIMING 14
#define SIN_FRACTION_BITS 8

#define PIN_MOT_EN  18
#define PIN_MOT_IN1 21
#define PIN_MOT_IN2 19
#define PIN_MOT_PWM 5

#define PIN_LED 23

uint16_t sinTableStep = 1;
uint16_t sinTablePos = 0;

uint16_t sinAmplitude = 30;

uint16_t ledPos = 0;
uint16_t ledStep = 1;
uint16_t ledDuty = 1;

// Next step: implement look-up table with interpolation. How to do look-up with integer counter? 
// IDEA: use FFT on scope to check frequency contents! 

// Next step: fix buggy interpolation implementation
// Implement amplitude control
// Switch to 16 bit sine values, and 16 bit fractional value? 


// generated using https://daycounter.com/Calculators/Sine-Generator-Calculator.phtml
const uint16_t SINE_LOOKUP_TABLE[] =
{
	// 128,152,176,198,218,234,245,253,
	// 255,253,245,234,218,198,176,152,
	// 128,103,79,57,37,21,10,2,
	// 0,2,10,21,37,57,79,103,
	32768,34825,36874,38908,40916,42893,44830,46719,48553,50325,52028,53654,55198,56654,58015,59277,
	60434,61482,62416,63234,63931,64506,64955,65277,65470,65535,65470,65277,64955,64506,63931,63234,
	62416,61482,60434,59277,58015,56654,55198,53654,52028,50325,48553,46719,44830,42893,40916,38908,
	36874,34825,32768,30710,28661,26627,24619,22642,20705,18816,16982,15210,13507,11881,10337,8881,
	7520,6258,5101,4053,3119,2301,1604,1029,580,258,65,0,65,258,580,1029,
	1604,2301,3119,4053,5101,6258,7520,8881,10337,11881,13507,15210,16982,18816,20705,22642,
	24619,26627,28661,30710,32768
};

void setSineFreq(float freq) {
  float sinTableStepF = (freq * SIN_TABLE_SIZE * (1<<SIN_FRACTION_BITS))/STEP_FREQUENCY;
  sinTableStep = (uint16_t) sinTableStepF;
  Serial << freq << "\t" << sinTableStepF << "\t" << sinTableStep << endl;
}

void setLedFreq(float freq) {
  float stepF = (freq * SIN_TABLE_SIZE * (1<<SIN_FRACTION_BITS))/STEP_FREQUENCY;
  ledStep = (uint16_t) stepF;
  Serial << freq << "\t" << stepF << "\t" << ledStep << endl;
}

void IRAM_ATTR ledc_timer0_overflow_isr(void *arg)
{
  uint16_t sinValue1, sinValue2;
  int32_t sinValueInt; 
  int32_t sinValueIntAmpl;

  digitalWrite(PIN_TIMING, 1);
	// clear the interrupt
	REG_SET_BIT(LEDC_INT_CLR_REG, LEDC_HSTIMER0_OVF_INT_CLR);

  // Compute integer and fractional parts of lookup table position
  uint16_t wholeStep = sinTablePos>>SIN_FRACTION_BITS;
  uint16_t fractStep = sinTablePos & ((1<<SIN_FRACTION_BITS)-1);
  sinValue1 = SINE_LOOKUP_TABLE[wholeStep];

  uint16_t nextInd = wholeStep + 1;
  if (nextInd>=SIN_TABLE_SIZE) nextInd = 0;
  sinValue2 = SINE_LOOKUP_TABLE[nextInd];

  // Interpolate
  sinValueInt = sinValue1 + ((sinValue2-sinValue1)*fractStep)/((1<<SIN_FRACTION_BITS)-1);

  sinValueIntAmpl = ((sinValueInt-32768) * sinAmplitude) / 255;
  sinValueIntAmpl += 32768;

  // Update duty cycle register. Value is << 4 (full resolution can be used, this will result in dithering, which is now not desired)
	// Lookup table is 16 bits. DUTY_REG expects a value that is << 4 (unless dithering is required). 
  // We have a 12 bit PWM resolution, so no shifting needed
	// REG_WRITE(LEDC_HSCH0_DUTY_REG, sinValueIntAmpl >> 4);
	REG_WRITE(LEDC_HSCH0_DUTY_REG, sinValueIntAmpl >> 1);
	REG_SET_BIT(LEDC_HSCH0_CONF1_REG, LEDC_DUTY_START_HSCH0);
	// REG_WRITE(LEDC_HSCH1_DUTY_REG, sinValueIntAmpl >> 4);
	REG_WRITE(LEDC_HSCH1_DUTY_REG, sinValueIntAmpl >> 1);
	REG_SET_BIT(LEDC_HSCH1_CONF1_REG, LEDC_DUTY_START_HSCH1);

  digitalWrite(PIN_TIMING, 0);

  // Serial << "\t" << wholeStep << "\t" << fractStep << "\t" << sinValue1 << "\t" << sinValue2 << "\t" << sinValueInt << endl;
  // Serial.println(sinValueInt);

  // Update table position
  sinTablePos += sinTableStep; 
  if ((sinTablePos>>SIN_FRACTION_BITS)>=SIN_TABLE_SIZE) sinTablePos -= (SIN_TABLE_SIZE<<SIN_FRACTION_BITS);
	
  // LED control
  // If at step zero, turn on 
  // If at duty cycle, turn off
  ledPos += ledStep; 
  if ((ledPos>>SIN_FRACTION_BITS)>=SIN_TABLE_SIZE) {
    ledPos -= (SIN_TABLE_SIZE<<SIN_FRACTION_BITS);
    digitalWrite(PIN_LED, 1);
  } else if((ledPos>>SIN_FRACTION_BITS)>10) {
    digitalWrite(PIN_LED, 0);
  }
}


void setup() {
  Serial.begin(115200);

  pinMode(PIN_TIMING, OUTPUT);
  pinMode(PIN_MOT_EN, OUTPUT);
  // pinMode(PIN_MOT_IN1, OUTPUT);
  // pinMode(PIN_MOT_IN2, OUTPUT);
  pinMode(PIN_MOT_PWM, OUTPUT);
  pinMode(PIN_LED, OUTPUT);

  digitalWrite(PIN_MOT_EN, 1);
  // digitalWrite(PIN_MOT_IN1, 0);
  // digitalWrite(PIN_MOT_IN2, 1);
  digitalWrite(PIN_MOT_PWM, 1);
  digitalWrite(PIN_LED, 0);

  // configure ledc timer0
	ledc_timer_config_t ledc_timer0;
	ledc_timer0.duty_resolution = LEDC_TIMER_11_BIT;
	ledc_timer0.freq_hz = STEP_FREQUENCY;
	ledc_timer0.speed_mode = LEDC_HIGH_SPEED_MODE;
	ledc_timer0.timer_num = LEDC_TIMER_0;
	ledc_timer_config(&ledc_timer0);
	
	// configure ledc channel0
	ledc_channel_config_t ledc_conf;
	ledc_conf.channel = LEDC_CHANNEL_0;
	ledc_conf.duty = 0;
	ledc_conf.gpio_num = PIN_MOT_IN1;
	ledc_conf.hpoint = 0;
	ledc_conf.timer_sel = LEDC_TIMER_0;
	ledc_conf.speed_mode = LEDC_HIGH_SPEED_MODE;
	ledc_conf.intr_type = LEDC_INTR_DISABLE;
  ledc_conf.flags.output_invert = 0;
	ledc_channel_config(&ledc_conf);

  
	// configure ledc channel1
	ledc_conf.channel = LEDC_CHANNEL_1;
	ledc_conf.duty = 0;
	ledc_conf.gpio_num = PIN_MOT_IN2;
	ledc_conf.hpoint = 0;
	ledc_conf.timer_sel = LEDC_TIMER_0;
	ledc_conf.speed_mode = LEDC_HIGH_SPEED_MODE;
	ledc_conf.intr_type = LEDC_INTR_DISABLE;
  ledc_conf.flags.output_invert = 1;
	ledc_channel_config(&ledc_conf);

	// configure ledc channel2
	// ledc_conf.channel = LEDC_CHANNEL_2;
	// ledc_conf.duty = 0;
	// ledc_conf.gpio_num = PIN_LED;
	// ledc_conf.hpoint = 0;
	// ledc_conf.timer_sel = LEDC_TIMER_0;
	// ledc_conf.speed_mode = LEDC_HIGH_SPEED_MODE;
	// ledc_conf.intr_type = LEDC_INTR_DISABLE;
  // ledc_conf.flags.output_invert = 0;
	// ledc_channel_config(&ledc_conf);
	
	// register overflow interrupt handler for timer0
	ledc_isr_register(ledc_timer0_overflow_isr, NULL, ESP_INTR_FLAG_IRAM, NULL);
	// enable the overflow interrupt
	REG_SET_BIT(LEDC_INT_ENA_REG, LEDC_HSTIMER0_OVF_INT_ENA);

  setSineFreq(SIN_FREQUENCY);
  setLedFreq(SIN_FREQUENCY + 1);

}

void loop() {
  if (Serial.available()>2) {
    delay(100);
    char cmd = Serial.read();
    float val = 123.456;
    if (Serial.available()) {
      val = Serial.parseFloat();
      switch (cmd) {
        case 'a':
          sinAmplitude = (uint16_t) val;
          break;
        case 'f':
          setSineFreq(val);
          break;
        case 'g':
          setLedFreq(val);
          break;
        case 'h':
          setSineFreq(val);
          setLedFreq(val+1);
          break;
      }
    }
    Serial << cmd << " " << val << endl;
    while (Serial.available()) Serial.read();
  }
}